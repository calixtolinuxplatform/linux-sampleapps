#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int
ADC_Read(int channel)
{
#define VALUE_MAX 30
	char path[VALUE_MAX];
	char value_str[4];
	int fd;
 
	snprintf(path, VALUE_MAX, "/sys/bus/iio/devices/iio\\:device0/in_voltage%d_raw", channel);
	fd = open(path, O_RDONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open %s for reading!\n",path);
		return(-1);
	}
 
	if (-1 == read(fd, value_str, 4)) {
		fprintf(stderr, "Failed to read value!\n");
		return(-1);
	}
 
	close(fd);
 
	return(atoi(value_str));
}

int main()
{

    while(1)
	{
		printf("\r\n adc channel 5 value = %d",ADC_Read(5));
		sleep(1);
		printf("\r\n adc channel 5 value = %d",ADC_Read(5));
		sleep(1);
	}

 return 0;
}
