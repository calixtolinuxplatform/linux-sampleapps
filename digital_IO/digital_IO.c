#include <stdlib.h>
#include <stdio.h>
#include "sysfs_gpio.h"


int main()
{

    /* Initialising GPIO2_24 and GPIO2_25 as input */
    GPIOInit(2,24,IN);
    GPIOInit(2,25,IN);

   /* Initialising GPIO2_22 and GPIO2_23 as output */    
    GPIOInit(2,22,OUT);
    GPIOInit(2,23,OUT);

    while(1)
	{
		printf("\r\n GPIO2_24 value = %d",GPIORead(2,24));
		printf("\r\n GPIO2_25 value = %d",GPIORead(2,25));

		GPIOWrite(2,22,LOW);
		GPIOWrite(2,23,LOW);
		sleep(1);

		GPIOWrite(2,22,HIGH);
		GPIOWrite(2,23,HIGH);
		sleep(1);
	}

 return 0;
}
