
#define IN  0
#define OUT 1
 
#define LOW  0
#define HIGH 1

extern int GPIOInit(int bank,int gpio,int dir);
extern int GPIORead(int bank,int gpio);
extern int GPIOWrite(int bank,int gpio, int value);
